#!/bin/bash

cd ..
mvn clean install
cp target/redis-app-1.0-SNAPSHOT.jar build/App && cd build

docker-compose --project-name redis-boot-app up --build