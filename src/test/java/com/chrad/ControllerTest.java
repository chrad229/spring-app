package com.chrad;

import com.chrad.controllers.IndexController;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.hamcrest.Matchers.containsString;

@RunWith(SpringRunner.class)
@WebMvcTest(IndexController.class)
@Ignore
public class ControllerTest {
    @Autowired
    ApplicationContext context;

    @Before
    public void prepare() {
//        Mockito.when(context.getBean())
    }

    @Autowired
    private MockMvc mock;

    @Test
    public void testController() throws Exception {
        mock.perform(get("/")).andExpect(status().isOk()).andExpect(view().name("home")).
                andExpect(content().string(containsString("Hello chumbas")));
    }
}
