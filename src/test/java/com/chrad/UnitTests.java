package com.chrad;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

@RunWith(SpringRunner.class)
@SpringBootTest
@Ignore
public class UnitTests {
    @Autowired
    ApplicationContext context;
    @Test
    public void test() {
        System.out.println(Arrays.toString(context.getBeanDefinitionNames()));
    }
}
