package com.chrad;

import com.chrad.data.Fruit;
import com.chrad.data.Recipe;
import com.chrad.repository.FruitRepository;
import com.chrad.repository.RecipeRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DbTest {
    @Autowired
    RecipeRepository recipeRepository;

    @Autowired
    FruitRepository fruitRepository;

    @Test
    public void testInsertRecipe() {
        fruitRepository.save(new Fruit(null, "Apple", null));
        fruitRepository.save(new Fruit(null, "Plum", null));
        List<Fruit> fruits = new ArrayList<>(fruitRepository.getAll());
        Assert.assertEquals(2, fruits.size());

        recipeRepository.save(new Recipe(null, fruits, null, "Fruit dessert", "Consists of 2 fruits"));
        List<Recipe> recipes = recipeRepository.getAll();
        Assert.assertEquals(1, recipes.size());
        System.out.println("recipe: "+recipes.get(0));
    }
}
