package com.chrad.data;

import lombok.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Fruit {
    private Long id;

    @NotBlank(message = "Name is required")
    @Size(min = 4, max = 50, message ="Name must be 5-50 length")
    @Pattern(regexp = "^[A-z ]+$", message = "incorrect symbols")
    private String name;

    private Date createdAt;
}
