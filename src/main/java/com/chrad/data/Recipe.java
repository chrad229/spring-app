package com.chrad.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Recipe {
    private Long id;
    private List<Fruit> fruits;
    private Date createdAt;
    @NotBlank
    private String name;
    private String description;
}
