package com.chrad.repository;

import com.chrad.data.Fruit;

import java.util.Collection;

public interface FruitRepository {
    Collection<Fruit> getAll();
    Fruit getOne(Long id);
    Long save(Fruit fruit);
    void delete(Long id);
}
