package com.chrad.repository;

import com.chrad.data.Recipe;

import java.util.List;

public interface RecipeRepository {
    List<Recipe> getAll();
    Recipe getOne(Long id);
    Long save(Recipe catalogue);
}
