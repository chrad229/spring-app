package com.chrad.repository;

import com.chrad.data.Fruit;
import com.chrad.data.Recipe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.*;

@Repository
public class JdbcRecipeRepository implements RecipeRepository {

    @Autowired
    JdbcTemplate jdbc;

    @Autowired
    FruitRepository fruitRepository;

    @Override
    public List<Recipe> getAll() {
        List<Recipe> recipes = jdbc.query("select id, name, description, createdAt from Recipe", this::recipeRowMapper);
        for (Recipe recipe : recipes) {
            List<Fruit> fruits = jdbc.query(
                    "select rf.fruitId, f.name, f.createdAt from RecipeFruit rf inner join Fruit f on rf.fruitId=f.id where rf.recipeId=?",
                    (set, cnt) -> new Fruit(set.getLong(1), set.getString(2), set.getTimestamp(3)),
                    recipe.getId());
            recipe.setFruits(fruits);
        }
        return recipes;
    }

    @Override
    public Recipe getOne(Long id) {
        List<Recipe> recipes = jdbc.query("select id, name, description, createdAt from Recipe", this::recipeRowMapper);
        if (recipes.isEmpty()) {
            return null;
        }
        Recipe recipe = recipes.get(0);
        List<Fruit> fruits = jdbc.query(
                "select rf.fruitId, f.name, f.createdId from RecipeFruit rf inner join Fruit f on rf.fruitId=f.id where rf.recipeId=?",
                (set, cnt) -> new Fruit(set.getLong(1), set.getString(2), set.getTimestamp(3)),
                recipe.getId());
        recipe.setFruits(fruits);
        return recipe;
    }

    @Override
    public Long save(Recipe recipe) {
        recipe.setCreatedAt(new Date());
        PreparedStatementCreatorFactory fact = new PreparedStatementCreatorFactory(
                "insert into Recipe (name, description, createdAt) values (?, ?, ?)",
                Types.VARCHAR, Types.VARCHAR, Types.TIMESTAMP);
        fact.setReturnGeneratedKeys(true);
        GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
        jdbc.update(fact.newPreparedStatementCreator(Arrays.asList(recipe.getName(), recipe.getDescription(), recipe.getCreatedAt())),
                generatedKeyHolder);
        recipe.setId(Objects.requireNonNull(generatedKeyHolder.getKey(), "Empty inserted id of Recipe").longValue());
        for (Fruit fruit : recipe.getFruits()) {
            jdbc.update("insert into RecipeFruit (fruitId, recipeId) values (?, ?)", fruit.getId(), recipe.getId());
        }
        return recipe.getId();
    }

    Recipe recipeRowMapper(ResultSet set, int rowCount) throws SQLException {
        return new Recipe(set.getLong("id"), new ArrayList<Fruit>(), new Date(set.getTimestamp("createdAt").getDate()),
                set.getString("name"), set.getString("description"));
    }
}
