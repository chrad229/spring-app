package com.chrad.repository;

import com.chrad.data.Fruit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.*;

@Slf4j
@Repository
public class JdbcFruitRepository implements FruitRepository {
    @Autowired
    private JdbcTemplate jdbc;

    @Override
    public Collection<Fruit> getAll() {
        log.info("select all");
        return jdbc.query("select id, name, createdAt from Fruit", this::mapFruitFromRow);
    }

    @Override
    public Fruit getOne(Long id) {
        List<Fruit> fruitList = jdbc.query("select id, name, createdAt from Fruit where id=?", this::mapFruitFromRow, id);
        log.info("get {}", fruitList);
        if (fruitList.isEmpty()) {
            return null;
        } else {
            return fruitList.get(0);
        }
    }

    @Override
    public Long save(Fruit fruit) {
        Fruit old = null;
        if (fruit.getId() != null) {
            old = getOne(fruit.getId());
        }
        Long id;
        if (old == null) {
            fruit.setCreatedAt(new Date());
            PreparedStatementCreatorFactory statement = new PreparedStatementCreatorFactory(
                    "insert into Fruit (name, createdAt) values (?, ?)",
                    Types.VARCHAR, Types.TIMESTAMP);
            statement.setReturnGeneratedKeys(true);
            GeneratedKeyHolder holder = new GeneratedKeyHolder();
            jdbc.update(statement.newPreparedStatementCreator(Arrays.asList(fruit.getName(), fruit.getCreatedAt())), holder);
            id = Objects.requireNonNull(holder.getKey(), "Generated key is null").longValue();
            log.info("inserted with id {} object {}", id, fruit);
        } else {
            jdbc.update("update Fruit set name='?' where id=?", fruit.getName(), fruit.getId());
            id = fruit.getId();
            log.info("updated object {}", fruit);
        }
        return id;
    }

    private Fruit mapFruitFromRow(ResultSet set, int rowNum) throws SQLException {
        return new Fruit(set.getLong("id"), set.getString("name"), set.getTimestamp("createdAt"));
    }

    @Override
    public void delete(Long id) {
        jdbc.update("delete from Fruit where id=?", id);
        log.info("deleted Fruit with id ={}", id);
    }
}
