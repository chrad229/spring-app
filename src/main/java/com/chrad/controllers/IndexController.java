package com.chrad.controllers;

import com.chrad.data.Fruit;
import com.chrad.repository.FruitRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Slf4j
@Controller
public class IndexController {
    @Autowired
    FruitRepository fruitRepository;

    @GetMapping("/")
    String home(Model model) {
        model.addAttribute("fruits", fruitRepository.getAll());
        model.addAttribute("fruit", new Fruit());
        return "home";
    }

    @PostMapping("/add")
    String processHome(@Valid Fruit fruit, Errors errors, Model model) {
        if (!errors.hasErrors()) {
            Long id = fruitRepository.save(fruit);
            log.info("added {}", id);
        } else {
            log.debug("error: {}", errors.getAllErrors());
            if (!errors.getFieldErrors("name").isEmpty())
            model.addAttribute("errors", errors.getFieldErrors("name"));
        }
        return "redirect:/";
    }

    @PostMapping("/remove")
    String processDelete(Long id) {
        fruitRepository.delete(id);
        log.info("deleted {}", id);
        return "redirect:/";
    }

    @PostMapping("/update")
    String processUpdate(@Valid Fruit f, Errors errors) {
        if (!errors.hasErrors()) {
            fruitRepository.save(f);
            log.info("updated {}", f);
        } else {
            log.error("errors update Fruit: {}", errors);
        }
        return "redirect:/";
    }
}
