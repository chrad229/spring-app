create table if not exists Fruit (
    id identity,
    name varchar(100) not null,
    createdAt timestamp
);

create table if not exists Recipe (
    id identity,
    name varchar(50) not null,
    description varchar(500) not null,
    createdAt timestamp not null
);

create table if not exists RecipeFruit (
    fruitId bigint not null,
    recipeId bigint not null,
    constraint pk_recipeFruit primary key (fruitId, recipeId),
    foreign key (fruitId) references Fruit(id),
    foreign key (recipeId) references Recipe (id)
);


